# Assemblée Générale Ordinaire 2018 du collectif Conscience

![theo](https://i.imgur.com/RJyzpFE.jpg)

16 Décembre 2018 - Centre de Recherche Interdisciplinaire (merci Pleen !)

[collectifconscience.org](https://collectifconscience.org)

---

# Quorum

- 11 membres présents sur place : 
> Eléonore, Étienne, Thibault, Sébastien, Lise, Valentine D, Valentine F, Sophie, Hervé, Denis, Théo, Nicolas
- 2 membres présents sur Discord :
> Jérémy et Lucile, qui est partie en cours de route et a donné sa procuration à Sophie
- Procurations : 
> * Seb (Pierre Kerner, Héloise Dufour, Pleen, Cécile Michaut, Tabatha Sauvaget)
> * Sophie (Charlotte H, Mathéo sauf VP, Lucile)
> * Etienne (Mathéo pour VP)
> * Valentine D (Aurélie Froger, Lê NGuyen Hoang, Hans Bodart)
> * Nicolas (Walter H)
> * Hervé (Margaux)

- 27 membres sont donc représentés à cette AGO. 
- Le quorum étant fixé à un tiers des membres actifs (45 membres actifs en tout), il est atteint !

---

### Sommaire
1. Projets en cours (et autres nouveautés)
1. Charte éthique + vote
1. Projets à venir
1. Projets en stand by
1. Vote du bilan moral
1. Point sur les financements
1. Bilan financier + vote
1. Révision des statuts + vote
1. Révision du règlement intérieur + vote
1. Élection du bureau 2019
1. Élection du Conseil d'administration 2019

---

### Projets en cours

---

- ***En Direct du Labo*** (présenté par Sébastien)
> * Août 2018: @Endirectdulabo atteint les 10k followers (Décembre 2018: 10,8k)
> * Nouveau site terminé ! 528 visiteurs depuis Août 2018
> * *A faire:* faire connaître le site aux lycées (réseaux: Canopée ? La main à la pâte ?)
- Sophie propose de renvoyer sur le site internet à la fin de chaque semaine pour augmenter le nombre de visiteurs sur le site (mais les curateurs doivent faire la fiche chaque semaine du coup). Elle a des contacts des inspecteurs pédagogiques régionaux 
- Denis connaît 2 mailing lists de profs pour transmettre @Endirectdulabo
 
---

- ***Science Shakers*** (présenté par Hervé)
> * Événement mensuel de promotion de la culture scientifique
> * Rejoint le collectif en Septembre 2018 (Discussions préliminaires en Mai)
> * 3 soirée organisées depuis :
> * "PARLONS MÉTIERS" (25 septembre 2018, Les Caractères)
> * "SOIRÉE SPÉCIALE ZOMBIES" (30 Octobre 2018, The Highlander)
> * "PEUT-ON DÉMOCRATISER L’OPEN SCIENCE?" (24 Novembre 2018, Deux Point Zéro)
> * ***Prochain événement*** : Science Shakers de Noël ! Mercredi 19 Décembre à La Maison

---

- ***Au Bar Sci*** (présenté par Nicolas)
> 4 soirées réalisées, avec divers formats testés (conférences -> format hybride proposé par Nicolas) 
> * "La science de la communication des sciences", avec Sébastien Carassou (20 mars 2018, French Guinguette)
> * "Science et (dés)information", avec Florian Gouthière (22 mai 2018, O'Mok Bar)
> * [Soirée à la French Guinguette](https://gitlab.com/collectif-conscience/aubarsci/wikis/Pr%C3%A9sentation%202018-10-04) (4 octobre 2018)
> * [Soirée à l'Institut des systèmes complexes](https://gitlab.com/collectif-conscience/aubarsci/wikis/Pr%C3%A9sentation%202018-11-14) (14 novembre 2018)


* Vu que Science Shaker nous a rejoint, Au Bar sci s'est transformé en événement interne au collectif, dont le but est d'échanger des nouvelles sur la culture scientifique, présenter des projets (perso ou autre) à l'association. 
* Pour proposer une intervention au prochain Au Bar Sci, demander à Nicolas (NicolasPA#1858 sur Discord) ou envoyer un message sur le salon #aubarsci.

---

- ***Charte éthique***

- Le prototype de charte suivant a été discuté : 
> * Cette charte décrit l’esprit de l’association collectif Conscience dont l’objet est de promouvoir et de diffuser la culture scientifique sous toutes ses formes. Toute collaboration avec l’association collectif Conscience suppose l’adhésion aux principes et aux valeurs de celle-ci, et l’engagement à les respecter.

> * tout association avec des partenaires doit correspondre aux valeurs de Conscience.

> * ***Démocratie :*** les valeurs démocratiques sont au cœur des processus du collectif Conscience. Tous les membres sont libres d’exprimer leur point de vue librement.
> * ***Transparence :*** le collectif Conscience s’engage à communiquer à ses membres et au public une information globale, claire et cohérente sur l’ensemble de ses activités et de ses sources de financements.
> * ***Développement durable :*** le collectif Conscience s’engage à faire preuve d'exigence sur la démarche éthique et durable de nos fournisseurs et partenaires, dans la mesure où ses modestes moyens associatifs le permettent.

---

### Vote de la charte éthique

***Vote à main levée des points de la charte:***
- ***Démocratie :*** ok à l'unanimité - 1
- ***Transparence*** (les partenaires éventuels doivent être visibles, on ne les cache pas volontairement : ok à l'unanimité
- ***Développement durable*** (dans la mesure des moyens) : Changement dans la phrase: le "nos fournisseurs" en "ses fournisseurs" : unanimité - 1

- Proposition de Denis : ajout de l'idée de notoriété 
> * ***Notoriété***:  à titre de support de sa notoriété, le Collectif Conscience accepte de soutenir la publicité de toute personne, toutes association ou groupe de communication destinées à la diffusion de la culture scientifique auprès du public déjà connues sur la Toile et parmi les réseaux sociaux et qui en feront la demande.
- la proposition est rejetée à l'unanimité - 1

***Vote à main levée de la charte:***
- La charte éthique est votée à l'unanimité - 1
- Un point sur la charte éthique devra être inscrit dans les nouveaux statuts

---

### Autres nouveautés

---

- 22 nouveaux membres inscrits sur la mailing list en 2018 (79 au total)
- 45 membres au total sur le Discord (mais seulement 15 se sont présentés :/)
> Ça serait bien que les gens se présentent sur le Discord (background, compétences). Parce qu'une des idées derrière le collectif c'est de mettre en relation des compétences.
- Nouvelle charte graphique de notre site (Jérémy), avec un dossier sur le Google Drive du Bureau.
> Ça serait bien d'avoir un bouton "contact" sur le site que les gens puissent contacter directement le Bureau par mail.

---

- Activation de notre compte Gsuite (Théo et Jérémy)
> Celles et ceux qui le demandent peuvent avoir une adresse @collectifconscience.org. Gsuite permet aussi de faire des listes de diffusion, un agenda collectif et un Drive collectif (accessible avec notre adresse @collectifconscience.org, avec des droits d'accès réglables selon les membres). Cela donne aussi la possibilité d'avoir des adresses mail génériques (par poste du Bureau, cessibles après chaque AG si un membre change de poste), ainsi qu'une chaîne YouTube avec plusieurs admins/modérateurs 
> * Si des membres veut en savoir plus, faire signe à Théo (Théo Sil#7107 sur Discord).

---

- Conscience fait partie de l'incubateur de projets de médiation du CNRS via Sophie
> * L'incubateur a été lancé cette année par l'Institut des systèmes complexes (CNRS + AMCSTI). 
> * Les scientifiques proposent leurs sujets de recherche à des gens de tout bord (musées, journalistes, indépendants, médiateurs...), et on a 9 mois pour construire un projet de médiation autour d'un des sujets de recherche. 
> * News : Les sujets de recherche ont été échangés (Sophie est membre de l'incubateur via le CNRS ET Conscience), et Sophie va construire une expo dont vous êtes le héros autour du langage. 
> * But final : au prochain forum NIMS (organisé par le CNRS), les projets seront présentés, et ils vont nous aider à trouver des partenaires et des financements pour le projet.
> * Groupe de Sophie : 2 scientifiques, 3 médiateurs.
> * Sophie va créer un channel dédié sur le Discord et en parler au prochain AuBarSci.
> * Eléonore est aussi dans l'incubateur en tant que chercheuse, dans un autre projet chapeauté par Margaux. 
> * Le Collectif Conscience est donc triplement présent au sein de cet incubateur dans deux équipes différentes !

---

- Nouvel outil de gestion de projets du collectif Conscience : Gitlab (Nicolas)
> Gitlab est généralement utilisé pour l'organisation du travail. On peut avoir des projets publics ou privés. Gitlab dispose d'onglet pour mettre des tickets, de fonctions de wiki, de fonctions pour transformer automatiquement une fiche Markdown en présentation (slides)... On peut se logger avec un compte Github, Facebook ou Google.
- Débat sur l'utilisation des suites Google versus les alternatives libres.

--- 

### Projets à venir

---

***Science Shakers et Au Bar Sci continuent !***

***Les Goodies :***
- Peuvent constituer une bonne source de financement pour renflouer les caisses !
- On pourrait aussi profiter des Science Shakers pour vendre des goodies de notre partenaire le Café des sciences

---

- ***T-shirts :*** 
- Il nous faut des T-shirts collectif Conscience pour porter en convention/festival par exemple. Pareil pour Science Shakers.
> - Il faut trouver un/des graphistes, idéalement bénévoles (des volontaires ? cf le salon #flyers-goodies-tshirts-etc sur Discord !)
> - Proposition de Théo : faire des T-shirts à thème. Pourquoi pas des T-shirts Science Shakers suivant le thème des conférences ? 
> - T-shirts thématiques ou T-shirt standard qui mélange plein de thématiques évoquées par Science Shakers ? T-shirt collector en fin d'année pour le dernier Science Shakers à thème libre ? Il y a un problème du rythme de production et de livraison si on doit faire un T-shirt différent par événement : gros boulot pour le graphiste et la personne qui effectue les commandes. Prévoir un paquet de graphistes bénévoles pour faire un roulement ? On commencera par juste 2 ou 3 designs, et on verra plus tard si on veut en faire plus. 
> - Comment passer les commandes ? Par un lien HelloAsso ? Une case à cocher "un T-shirt vous intéresserait ?" sur le formulaire d'inscription à Science Shakers ?
> - ***Impression des T-shirts :*** artesan.fr (contact aux Grands Voisins) envisager un partenariat avec eux pour faire baisser les prix en échange de faire sa pub ? Voir aussi la définition de l'impression. Aller les voir directement ?
> - ***Prix des T-shirts :*** 15 euros max pour les membres et 18 euros hors membres ? Il faudrait faire un sondage sur les réseaux sociaux pour demander au public combien il serait prêt à payer pour un T-shirt de bonne qualité, éthique autant que possible, avec un design exclusif. Ou bien un sondage interne (sur Discord et mailing-list).
> - ***Slogan des T-shirts :*** Lise propose une idée de slogan "science sans collectif conscience n'est que ruine de l'âme" (mais Nicolas n'est pas trop fan parce que cette citation n'a pas été formulée dans un contexte rationnel à la base). Lise propose donc d'ajouter le disclaimer "mais en fait non parce que" en expliquant le contexte de la citation d'origine (sur les goodiez et sur le site web). Nicolas est d'accord.
> - Lise propose aussi des hoodies écrits "collectif conscience" façon université américaine

---

- ***Tote bags***
> - [Selfor](https://www.selforparis.com/fr/selfor/ est une bonne boutique gros/détail pour les tote-bags

- ***Cartes***
> * Il faudrait profiter des Science Shakers pour distribuer des petites cartes avec le nom de l'association et les liens vers le site et les réseaux sociaux (Mais et les déchets ? Qui garde les cartes après ?) + avoir une diapo qui résume les activités de Conscience à chaque événement.

- ***Mugs***
> - Valentine D propose des béchers en verrerie de labo ?
> - Lise propose des mugs thermo-réactifs où une image apparaît quand on met un liquide chaud dedans

---

- ***Paiement des goodies***
> - Est-ce que c'est légal que tout le monde donne une piécette à quelqu'un de Conscience (tirelire sur place), qui lui-même se chargerait de faire un don HelloAsso ? Hervé dit que oui. Mais il faut définir à l'avance le membre qui se charge de faire la tirelire, et aussi avoir une liste avec nom de la personne, montant versé et date (+signature ?).
> - Théo propose d'utiliser la plateforme Sumup (et son terminal de paiement) pour transférer l'argent des goodies directement par carte bancaire (mais attention, besoin d'un Wifi potable pour ça).
> - Les goodies sont-ils déductibles des impôts ? On a droit à une réduction d'impôts car on est un association loi 1901 (sauf si les goodies représentent des contreparties !). Il y a une plus grosse réduction d’impôts si association est reconnue d'utilité publique (mais c'est difficile d'avoir ce statut). Sinon : éducation populaire.

---

***Calendrier collaboratif des événements scientifiques (initié par Valentine D)***
- Monter un petit groupe pour lancer un premier test de calendrier, via la plateforme de données de la boite de Lise (cf les discussions sur le salon Discord #evenements-sciences-ile-de-france) qui permettrait de se sourcer sur des calendriers ouverts et d'y ajouter les nôtres. Sébastien et Hervé sont partants pour faire partie du groupe.

---

***Festival Zombie (initié par Margaux)***
- L'événement consiste à utiliser le thème des zombies pour faire de la médiation scientifique, car c'est une thématique qui plait ! 
- Il faudrait organiser un événement qui dure plus qu'une soirée -> un festival ? 
- L'initiative pourrait être proposée à des appels à projets d'ici le prochain Halloween

---

***Mondes imaginaires (initié par Thibault)***
- Thibault aime beaucoup les cartes (les voir, les dessiner), y compris les cartes de mondes imaginaires, mais c'est rare qu'il y ait des éléments naturels réalistes 
- L'idée de cette initiative, c'est d'imaginer une carte imaginaire réaliste et collaborative, dans un système stellaire, en impliquant des scientifiques.
- Ça ne sera pas forcément un festival, mais un événement nouveau. 
> Durée : 2 journées ? scientifiques créent un monde et le montrent le 2ème jour
- Ce projet pourrait finir en maquette pédagogique utilisable par les profs, pourquoi pas en jeu de plateau, à lier avec des romans (Tolkien, Jordan)

---

***Manifeste du collectif Conscience (initié par Nicolas)***
- L'idée, c'est de décrire les valeurs du collectif sur un texte court et impactant
- Il regrouperait les notions de partage de connaissance, la mise en valeur de l'apport humain, la communication à tous, le regroupement de la science et de la culture, la participation de tous, l'esprit critique et de rationalité, ainsi que le constat que la science est peu représentée dans les médias,
- Sébastien propose de rajouter des éléments plus pragmatiques/institutionnels: qu'est ce qu'il manque pour améliorer la culture scientifique? 
- Le manifeste répond à la question du "pourquoi le collectif", et la charte éthique répond à la question du "comment ces valeurs sont implémentées"
- Le manifeste est consultable et modifiable via [le framapad ci-joint](https://mypads.framapad.org/p/manifeste-du-collectif-conscience-0n18bt74e). N'hésitez pas à le compléter !

---

***Marche pour la science 2019***
- Hervé, Margaux et Valentine F. sont issus du mouvement de la marche pour la science
- En 2019, une marche au niveau européen?

---

***Play Azur Festival*** 
- [Un festival Geek et scientifique](https://playazur.fr/) (un étage entier dédié à la science) qui se tiendra à Nice les 9 et 10 février 2018
- Il y a une possibilité d'avoir un stand gratuit au festival Play Azur, mais pas de défraiement possible pour le transport et l'hébergement.
- Conscience avait un stand en Janvier dernier, en partenariat avec le Café des Sciences.
- Si on y va, il nous faut un projet concret, pour être sûr d'être accepté par les organisateurs du festival
- Thibault, Seb, Nicolas, (Valentine D) sont déjà volontaires
- Thibault propose une idée d'animation: des énigmes scientifiques à résoudre le + rapidement possible, à la manière d'un escape game
(beaucoup d'expériences, issues de l'émission On est pas que des cobayes) 
- Nicolas propose une activité autour des big data : par exemple montrer les tweets de En direct du labo et demander de rattacher à des disciplines scientifiques ?
- Deadline pour proposer des activités : Fin Décembre.
> ***S'il y a d'autres volontaires pour tenir le stand de Conscience à Play Azur et/ou proposer des activités, manifestez-vous sur Discord ! (cf salon #festivals)***

---

***Bande annonce des activités de Conscience (initié par Sophie)***
- Sophie a écrit un script pour réaliser une bande annonce de présentation du collectif Conscience, histoire de donner envie aux gens de rejoindre l'association.

---

***Guide du vulgarisateur galactique 2.0 (initié par Valentine D)***
- C'est un guide d'astuces pratiques réalisé à 10 mains par les membres du collectif à destination des vulgarisateurs débutants. Il est [disponible sur notre site](https://collectifconscience.org/Le-Guide-du-Vulgarisateur-Galactique.pdf).
- Valentine a repris le guide existant (non mis en page) pour refaire une maquette plus aboutie
- La version 2.0 est prête sauf la couverture qui reste à finir 
- Sébastien se propose de trouver une couverture au guide rapidement (flat design)

---

***Chaîne Youtube***
- [La nouvelle chaine Youtube du collectif](https://www.youtube.com/channel/UCgyFCF_T8E6Px4s4GZpKLqA/) servira à l'avenir à diffuser les retransmissions des événements de Science Shaker. (abonne toi !) 
- Théo propose aussi de faire des Lives sur Facebook.

---

***Site web*** 
- Théo propose de revoir la page du site pour en faire un truc plus vivant, en intégrant par exemple les tweets d'@Endirectdulabo, le futur calendrier des événements, etc.
- Théo propose que sur le long terme, il faudrait aussi rapatrier les projets sur le site du Collectif Conscience (liens sur les sites web, et sur les comptes des RS ?).

---

### Projets en stand by

---

***Cephalus 3 (Valentine D)***
- [Cephalus](http://www.cephalusmag.fr/) est un magazine gratuit sur la culture scientifique
- Il compte actuellement 2 numéros, et une trentaine de contributeurs. Un 3e numéro est en préparation.
- Théo propose de donner une deadline aux projets. Après discussion, il semble moins contraignant de proposer une sorte de "barre d'avancement" (sous forme de tickets sur Gitlab par exemple), avec des sous-tâches attribuées aux différents acteurs.
- Dès que ce numéro est sorti, Valentine s'organise pour déléguer plus de tâches.
- Il existe une association qui fait distribuer des magazines par des gens dans le métro. On pourrait les contacter pour distribuer Cephalus ! Valentine est OK pour ce partenariat.

---

***Sciences en gare (Sophie)***
- C'est un projet qui a 3 ans, et qui consiste à mettre des scientifiques dans le RER pour qu'ils parlent aux gens pendant leur trajet. 
- La RATP ne voulait pas que ce soit dans les rames, mais dans les gares.
- L'Institut de biologie du CNRS veut faire un truc semblable, et le projet pourrait donc se faire pendant la fête de la science de cette année. Sophie va leur parler de notre version du projet.

---

***Troidet (Nicolas et Denis):*** 
- L'idée c'est d'avoir des petits goodies / maquettes imprimés en 3D, de produire des objets manipulables pour les animations. 
- Ex : éléphant en mousse sur un plan, on découpe les pièces et on l'assemble soi-même. A réaliser en FabLab. 
- Pour l'instant, rien de concret mais c'est en réflexion.
- Ça peut aussi entrer dans les contreparties. 

---

***Préservation du ciel nocturne en ville***
- Sophie propose une initiative à définir autour de la pollution lumineuse. 
- Denis propose de se rapprocher de [l'Association nationale de protection du ciel nocturne](https://www.anpcen.fr/).

---

***The Last Jeudi***
- Un rendez-vous régulier autour du thème du spatial tous les derniers Jeudi du mois
- Étienne propose un projet de collaboration avec The Last Jeudi et Space Up

--- 

### Bilan moral

---

***Conclusion du président Seb :*** 
- On a rarement (jamais ?) été aussi actifs que cette année, Sébastien est content. "Franchement, c'était cool". Saluons le travail remarquable des organisateurs de Science Shakers. On a réussi à remonter un peu la visibilité du collectif par rapport aux projets. Plus efficaces, plus grand, plus beaux.
- Théo : "Collectif Conscience est mieux."
- Par contre on est toujours aussi mauvais en recherche de financements, dit le trésorier.

***Priorités pour l'année prochaine :*** plus de communication externe, et plus de financements !

---

### Vote du bilan moral

Pour à l'unanimité - 1 (Jérémy s'abstient)

---

### Point sur les financements

- On a [un compte Hello Asso](https://www.helloasso.com/associations/collectif-conscience) ! (mais vide pour le moment...)
- Conscience possède un numéro de SIRET et peut donc demander des subventions
- Conscience est inscrit sur [le Système d'Information Multiservices des Partenaires Associatifs (SIMPA)](https://services-certifies.apps.paris.fr/simpa/ASSO/AUTH/p1.htm?MDP-WSSO-REQUEST=/simpa/ASSO/AUTH/p1.htm?MDP-WSSO-REQUEST=/simpa/ASSO/index.jsp&MDP-WSSO-SESSION=92df4f0e187a3376fe3c6903e378760b&MDP-WSSO-SESSION=3d0bdf01a26614cdffc23189ca0844d0&error=Votre%20session%20est%20expir%E9e%20-%20%20Veuillez%20vous%20r%E9authentifier&status=2) de la Mairie de Paris, et peut donc répondre aux appels à projets
- ***Proposition:*** vente de goodies (Tshirts, Tote bags) lors de nos événements
- Bons plans bienvenus !

---

### Bilan financier (Nicolas)

![tréso](https://i.imgur.com/ED4R3HX.png)

---

### Vote du bilan financier

- Pour : unanimité - 1 (Jérémy s'abstient)

---

### Révision des statuts 

- Modification des articles 8 et 10.
> Proposition d'Hervé sur art 10 : si un membre actif n'assiste pas ou n'est pas représenté à deux AG consécutives, il passe automatiquement en membre passif.
- Définition du siège social de l'association par le règlement intérieur.
- Proposition d'Hervé : ajouter dans le règlement intérieur comment on modifie la charte éthique. 
- Révision de l'article 2 : une charte éthique est mise en place, ses modalités sont précisées dans le règlement intérieur.

---

### Vote des nouveaux statuts

- Révision de l'article 3 : pour à l'unanimité -1 (Jérémy s'abstient)
- Révision de l'article 8 : pour à l'unanimité -2 (Abstention : 2 (Étienne et Jérémy))
- Révision de l'article 10 : pour à l'unanimité -1 (Jérémy s'abstient)
- Révision de l'article 2 : pour à l'unanimité -1 (Jérémy s'abstient)
- Vote des nouveaux statuts : pour à l'unanimité -1 (Jérémy s'abstient)

---

### Révision du règlement intérieur

***Changement de l'adresse du siège social de l'association***
> Le siège de l'association est établi chez Nicolas Parot-Alvarez, (adresse)

***Changement des postes du bureau et du CA  :***
- Bureau:
> 1. Président
> 1. Trésorier
> 1. Secrétaire
> 1. Chargé(e) de communication **externe**
> 1. Agent de liaison projets
> 1. Vice-président
> 1. Ambassadeur café des sciences (si le chargé de communication est déjà dans le café)
- Le Conseil d'Administration est composé d'une part des membres du Bureau qui le souhaitent et d'autre part de maximum 3 autres membres actifs élus lors d'une Assemblée générale ordinaire. 

---

### Vote du nouveau règlement intérieur

- Vote du nouveau siège : pour à l'unanimité -1 (Jérémy s'abstient)
- Changement du nombre de membres du CA et au Bureau : pour à l'unanimité -1 (Jérémy s'abstient)
- Changement des postes du bureau et du CA + des rôles dans le Bureau : pour à l'unanininimité -1 (Jérémy s'abstient)

- Vote du règlement intérieur : pour à l'unanimitité -1 (Jérémy s'abstient)

---

### Élection du bureau 2019

***Candidats :***
1. Président : Sébastien
> Contre : 0 Abstient : 1 Pour : le reste
2. Trésorier : Nicolas
> Contre : O Abstient : 2
3. Secrétaire : Denis
> Contre : 0 Abstient : 5
4. Chargé de communication externe : Théo
> Contre : 0 Abstient : 1
5. Agent de liaison projets : Margaux
> Contre : 0 Abstient : 2
6. Vice-président : Sophie
> Contre : 0 Abstient : 2
7. Ambassadeur café des science (si le chargé de communication est déjà dans le café) : NA

- ***Le bureau 2018-2019 est donc constitué de Sébastien Carassou (président), Sophie Félix (vice-présidente), Nicolas Parot (trésorier), Denis Chadebec (secrétaire), Théo Silvagno (chargé de communications) et Margaux Calon (agent de liaison projets) !***

---

### Élection du Conseil d'administration 2019

- ***Rappel :*** le CA est composé de :
> Membres du Bureau qui le souhaitent + 3 membres actifs max

- ***Candidats :***
> * Membre 1 : Hervé
> * Membre 2 : Thibault
> * Membre 3 : Valentine Facque

***Vote du CA***
> * Contre : 0 
> * Abstention : 4 
> * Pour : le reste

- ***Le CA 2018-2019 est donc constitué de tous les membres du bureau + Hervé Tram, Thibault Féral et Valentine Facque !***

---

Merci à tou(te)s d'avoir participé à cette AG qui restera dans l'histoire :)

![clafoutis](https://i.imgur.com/3BjlVui.jpg)

---

Transformer en slides: https://gitpitch.com/collectif-conscience/collectif/master?grs=gitlab
